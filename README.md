# Node.js Tutorial

+ I am told the best way to learn a language, like Javascript, or in this case, Node.js, is to do **code labs** and to contribute to **open-source** projects. I at the tutorial level at current. I posted a [blog post](https://billbunkum.github.io/blog.html) about the MEAN tutorial I went through. I'll post a couple more in the next month or two concerning my progress with Node.

## local env
+ nodeenv called `node-tutorial`

## Steps Taken
1. Setup
    + setup & download Node
    + install MongoDB
    + clone tutorial's 'example' branch into `seed` dir
2. Create 'authentication' endpoints
    + POST /api/users - registers users
    + POST /api/users/login - logging in users
    + GET /api/user - identifies user who's logged in and refreshes JWT token
    + PUT /api/user - updates user info.
    + **"Must"** create a `router` to do all of this: `routes/api/users.js`, this file **is* the `router`. As such, it must be registered in `routes/api/index.js`

### Rundown
1. Use Mongoose to create Schemas/Models
2. Create 'helper' methods on Models and 'route' Middleware
3. Create 'route' to 'expose' functionality to Users

## Blog thoughts
+ MongoDB
    1. NoSQL - uses Collections & Documents
    2. Docs w/in a Collection don't need to share the same Schema
+ Mongoose
    1. provides `callbacks` & `validations` to ensure consistent data
    2. Models made w/ Mongoose allow access of MongoDB in an OO fashion
    3. Create Schema for Model then register it w/ Mongoose
+ Passport (Middleware aka Plugins for Node)
    1. Another layer of abstraction, Passport is based off [Connect](https://github.com/senchalabs/connect#readme)
    2. Specifically provides **Authorization** and can work with [OAuth](https://oauth.net/)
    3. Docs [here](http://passportjs.org/docs)