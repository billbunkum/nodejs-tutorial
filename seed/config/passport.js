const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const User = mongoose.model('User');

// EXAMPLE
// "user": {
//    "email": "my@email.com"
//    "password": "mypassword"
// }
// usernameField: user[email]
// passwordField: user[password]

passport.use(new LocalStrategy({
// these are apparently NOT Strings
    usernameField: 'user[email]',
    passwordField: 'user[password]'
}, function(email, password, done) {
    User.findOne({email: email}).then(function(user){
        if(!user || !user.validPassword(password)){
            return done(null, false, {
                errors: {
                    'email or password': 'is invalid'
                }
            });
        }

        return done(null, user);
    }).catch(done);
}));