// creates (2) JWT middlwares for handling Required and Optional authorizations

var jwt = require('express-jwt');
var secret = require('../config').secret;

// route Middleware which decodes JWTs
function getTokenFromHeader(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token') {
        return req.headers.authorization.split(' ')[1];
    }
    return null; // 'null' signifies no errors?
}

let auth = {
    // req auth
    required: jwt({
        secret: secret,
        userProperty: 'payload',
        getToken: getTokenFromHeader
    }),
    // optional auth
    optional: jwt({
        secret: secret,
        userProperty: 'payload',
        credentialsRequired: false,
        getToken: getTokenFromHeader
    })
// access PAYLOAD data from each request from 'req.payload'
};

// exportin the 'auth' object
module.exports = auth;