var router = require('express').Router();

router.use('/', require('./users'));

//event handler uses 4 args, first is always error
router.use(function(err, req, res, next){
    if(err.name === 'ValidationError') {
        return res.status(422).json({
            errors: Object.keys(err.errors).reduce(
                function(errors, key){
                    errors[key] = err.errors[key].message;

                    return errors;
                }, {}) // .reduce( function(){}, {}) ??
        }); //use a ; cos this is a 'return' statement // .json( { errors: ~ });
    }

    return next(err);
});

module.exports = router;