// questions
const mongoose = require('mongoose');
const router = require('express').Router();
const passport = require('passport');
const User = mongoose.model('User');
const auth = require('../auth');

router.post('/users', function(req, res, next){
    let user = new User();

    user.username = req.body.user.username;
    user.email = req.body.user.email;
    user.setPassword = req.body.user.password;

    user.save().then(function(){
        return res.json({user: user.toAuthJSON()});
    }).catch(next); //next usually checks for next available route, is this 'catching' next() and stopping that route lookup?
});
//EXPLAINATION: user.save() returns a promise that needs handling. If resolved, the user was saved, now must return the user's auth JSON. If rejected, catch() passes and error to the 'error handler'. Currently, erros looks like HTTP 500. Must create a 'middleware' function to be an 'error handler' to make more readable error msgs.

// authenticates user at login & generates JWT
router.post('/users/login', function(req, res, next) {
    if(!req.body.user.email){
        return res.status(422).json({errors: {email: "can't be blank"}});
    }
    if(!req.body.user.password){
        return res.status(422).json({errors: {password: "can't be blank"}});
    }
    passport.authenticate('local', {session: false}, function(err, user, info){
        if(err){
            return next(err);
        }
        if(user){
            user.token = user.generateJWT();
            return res.json({user: user.toAuthJSON()});
        } else {
            return res.status(422).json(info); // what is the value of 'info'?
        }
    })(req, res, next); //why this? passport.authenticate()'(req, res, next)' ???
    // this seems to be a callback to 'done' w/in config/passport.js
});

//get user's payload from their token
router.get('api/user', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user){
            return res.sendStatus(401);
        }
        return res.json({user: user.toAuthJSON()});
    }).catch(next);
});

// allows user to update her information
router.put('/user', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user){
            return res.sendStatus(401);
        }
        // update fields passed and nothing else
        if(typeof req.body.user.username !== 'undefined'){
            user.username = req.body.user.username;
        }
        if(typeof req.body.user.email !== 'undefined'){
            user.email = req.body.user.email;
        }
        if(typeof req.body.user.bio !== 'undefined'){
            user.bio = req.body.user.bio;
        }
        if(typeof req.body.user.image !== 'undefined'){
            user.image = req.body.user.image;
        }
        if(typeof req.body.user.password !== 'undefined'){
            user.password = req.body.user.password;
        }

        return user.save().then(function(){
            return res.json({user: user.toAuthJSON()});
        });
    }).catch(next);
});

module.exports = router;