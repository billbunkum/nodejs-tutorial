"use strict";
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator'); // validates unique fields
const crypto = require('crypto'); // Node library which uses pbkdf2 alg.; need for setting & validating passwords
var jwt = require('jsonwebtoken');
var secret = require('../config').secret; // need a `secret` to sign & validate JWT's from Front-end; `secret` is basically a password for JWT's. It is set to 'secret' in dev and reads from an env variable in production

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        lowercase: true,
        unique: true,
        // required: [true, 'can\'t be blank'],
        match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
        index: true // optimizes queries which use the `username` field
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        // required: [true, 'can\'t be blank'],
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    bio: String,
    image: String,
    hash: String,
    salt: String
},
    {timestamps: true} // creates `createdAt` and `updatedAt` fields on models which are automatically updated
);

UserSchema.plugin(uniqueValidator, {
    message: 'is already taken.'
});

// Salting / hashing passwords
UserSchema.methods.setPassword = function(password){
    this.salt = crypto.randomBytes(16).toString('hex');
// pbkdf2Sync() takes 5 params: the password to hash, the salt, the iteration (# times hashed), the hashes' length, the alg. used
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

// validate passwords
UserSchema.methods.validPassword = function(password){
    var hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

// generates JWT for user
UserSchema.methods.generateJWT = function(){
    let today = new Date();
    let exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign({
        id: this._id,
        username: this.username,
        exp: parseInt(exp.getTime() / 1000),
    }, secret);
};

// gets JSON representation of user for authentication
UserSchema.methods.toAuthJSON = function(){
    return {
        username: this.username,
        email: this.email,
        token: this.generateJWT()
    };
};

mongoose.model('User', UserSchema); // call w/ `mongoose.model('User')`